package snhu.jukebox.playlist.tests;

import static org.junit.Assert.*;
import java.util.ArrayList;
import org.junit.Test;
import music.artist.*;
import snhu.jukebox.playlist.Song;

public class JukeboxTest {

	@Test
	public void testGetBeatlesAlbumSize() throws NoSuchFieldException, SecurityException {
		 TheBeatles theBeatlesBand = new TheBeatles();
		 ArrayList<Song> beatlesTracks = new ArrayList<Song>();
		 beatlesTracks = theBeatlesBand.getBeatlesSongs();
		 assertEquals(2, beatlesTracks.size());
	}
	
	@Test
	public void testGetImagineDragonsAlbumSize() throws NoSuchFieldException, SecurityException {
		 ImagineDragons imagineDragons = new ImagineDragons();
		 ArrayList<Song> imagineDragonsTracks = new ArrayList<Song>();
		 imagineDragonsTracks = imagineDragons.getImagineDragonsSongs();
		 assertEquals(3, imagineDragonsTracks.size());
	}
	
	@Test
	public void testGetAdelesAlbumSize() throws NoSuchFieldException, SecurityException {
		 Adele adele = new Adele();
		 ArrayList<Song> adelesTracks = new ArrayList<Song>();
		 adelesTracks = adele.getAdelesSongs();
		 assertEquals(3, adelesTracks.size());
	}
	
	//Test for newly added artist Foo Fighters -- MA -- 4-5-20
	@Test
	public void testGetFooFighetersAlbumSize() throws NoSuchFieldException, SecurityException {
		 FooFighters fooFighters = new FooFighters();
		 ArrayList<Song> fooFightersTracks = new ArrayList<Song>();
		 fooFightersTracks = fooFighters.getFooFightersSongs();
		 assertEquals(3, fooFightersTracks.size());
	}
	
	//Test for newly added artist Outkast -- MA -- 4-5-20
	@Test
	public void testGetOutkastAlbumSize() throws NoSuchFieldException, SecurityException {
		 Outkast outkast = new Outkast();
		 ArrayList<Song> outkastTracks = new ArrayList<Song>();
		 outkastTracks = outkast.getOutkastSongs();
		 assertEquals(2, outkastTracks.size());
	}
	
}
