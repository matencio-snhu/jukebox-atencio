package snhu.student.playlists;

import snhu.jukebox.playlist.PlayableSong;
import snhu.jukebox.playlist.Song;
import music.artist.*;
import java.util.ArrayList;
import java.util.LinkedList;

public class MattAtencio_Playlist {
    
	public LinkedList<PlayableSong> StudentPlaylist(){
	
	LinkedList<PlayableSong> playlist = new LinkedList<PlayableSong>();
   
	//add Foo Fighter songs
	FooFighters fooFightersBand = new FooFighters();
    ArrayList<Song> fooFightersTracks = new ArrayList<Song>();
    fooFightersTracks = fooFightersBand.getFooFightersSongs();
	
	playlist.add(fooFightersTracks.get(0));
	playlist.add(fooFightersTracks.get(1));
	playlist.add(fooFightersTracks.get(2));
	
	//add Outkast songs
    Outkast outkastBand = new Outkast();
	ArrayList<Song> outkastTracks = new ArrayList<Song>();
    outkastTracks = outkastBand.getOutkastSongs();
	
	playlist.add(outkastTracks.get(0));
	playlist.add(outkastTracks.get(1));
	
	//add Beatles song
	TheBeatles theBeatlesBand = new TheBeatles();
	ArrayList<Song> beatlesTracks = new ArrayList<Song>();	
    beatlesTracks = theBeatlesBand.getBeatlesSongs();
	
	playlist.add(beatlesTracks.get(0));
	playlist.add(beatlesTracks.get(1));
	
	
    return playlist;
	}
}
