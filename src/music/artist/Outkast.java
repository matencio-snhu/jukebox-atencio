package music.artist;

import snhu.jukebox.playlist.Song;
import java.util.ArrayList;

//New artist Outkast -- MA -- 4-5-20

public class Outkast {
	
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public Outkast() {
    }
    
    public ArrayList<Song> getOutkastSongs() {
    	
    	 albumTracks = new ArrayList<Song>();                                   //Instantiate the album so we can populate it below
    	 Song track1 = new Song("Roses", "Outkast");                            //Create a song
         Song track2 = new Song("Ms. Jackson", "Outkast");                      //Create another song
         this.albumTracks.add(track1);                                          //Add the first song to song list for the Outkast
         this.albumTracks.add(track2);                                          //Add the second song to song list for the Outkast
         return albumTracks;                                                    //Return the songs for the Outkast in the form of an ArrayList
    }
}
